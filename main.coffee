$ ->
  $('#debug').on 'change', ->
    $('html').toggleClass('debug')

  slideReset = ->
    $('.slideshow .scroll').css
      marginLeft: 0

  do initSlide = ->
    slideReset()
    $('.slideshow').each ->
      $('.scroll', this).css
        fontSize: ($(this).width() / 10.0) + 'px'
        lineHeight: 1

  slideNext = (slideshow) ->
    left = parseInt($('.scroll', slideshow).css('marginLeft'))
    width = $('.slide', slideshow).outerWidth(true)
    $('.scroll', slideshow).animate
      marginLeft: (left - width) + 'px'

  $('.slideshow').on 'click', ->
    slideNext(this)

  $(window).on 'resize', initSlide