<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" href="main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
</head>
<body>
    <hr>

    <div class="pad">
        <div class="slideshow">
            <div class="scroll">
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?1"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?2"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?3"></div>
                </div>
            </div>
        </div>

        <div class="slideshow three">
            <div class="scroll">
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?1"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?2"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?3"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?4"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?5"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?6"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?7"></div>
                </div>
                <div class="slide">
                    <div class="inner"><img src="https://placeimg.com/320/240/any?8"></div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="container">
        <div class="row">
            <?php foreach(range(1, 8) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row outside-spacing">
            <?php foreach(range(1, 4) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <div class="container outside-spacing">
        <div class="row">
            <?php foreach(range(1, 4) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <?php foreach(range(1, 4) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <div class="richards-special">
        <div class="row">
            <?php foreach(range(1, 8) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <div class="percent">
        <div class="row">
            <?php foreach(range(1, 8) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <div class="percent outside-spacing">
        <div class="row">
            <?php foreach(range(1, 8) as $i) : ?>
                <div class="col col-<?php echo $i; ?>">
                    <div class="content">
                        #<?php echo $i; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <div class="debug">
        <input id="debug" type="checkbox" name="debug">
        <label for"debug">Debug</label>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="main.js"></script>
</body>
</html>